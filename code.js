const form = document.getElementById("form"); // Se obtiene el formulario por su ID

window.onload = () => {
  document.querySelector(".send-button").addEventListener("click", e => { // Se agrega un evento al botón "send-button" para validar el formulario
    e.preventDefault(); // Se previene el comportamiento por defecto del botón
    validateForm(); // Se llama a la función que valida el formulario
  });
};

form.addEventListener("submit", e => { // Se agrega un evento al formulario para validar el formulario
  e.preventDefault(); // Se previene el comportamiento por defecto del formulario
  let condicion = validateForm(); // Se llama a la función que valida el formulario y se guarda el resultado en una variable
  if (condicion) { // Si el resultado de la validación es verdadero
    form.reset(); // Se reinicia el formulario
  }
});

function validateForm() { // Función que valida el formulario
  let condicion = true; // Se inicializa la variable condicion en verdadero
  const nombres = document.getElementById("nombres"); // Se obtiene el valor del campo "nombres"
  if (nombres.value.length < 1 || nombres.value.trim() == "") { // Si el campo está vacío o contiene solo espacios en blanco
    document.getElementById("n-error").innerHTML = "Nombre no válido"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  }
  const apellidos = document.getElementById("apellidos"); // Se obtiene el valor del campo "apellidos"
  if (apellidos.value.length < 1 || apellidos.value.trim() == "") { // Si el campo está vacío o contiene solo espacios en blanco
    document.getElementById("a-error").innerHTML = "Apellido no válido"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  }
  const edad = document.getElementById("edad"); // Se obtiene el valor del campo "edad"
  if (edad.value.length < 1 || edad.value.trim() == "") { // Si el campo está vacío o contiene solo espacios en blanco
    document.getElementById("ed-error").innerHTML = "edad no válida"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  }
  const Cedula = document.getElementById("Cedula"); // Se obtiene el valor del campo "Cedula"
  if (Cedula.value.length < 1 || Cedula.value.trim() == "") { // Si el campo está vacío o contiene solo espacios en blanco
    document.getElementById("C-error").innerHTML = "Numero de cedula no válido"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  }
  const Telefono = document.getElementById("Telefono"); // Se obtiene el valor del campo "Telefono"
  if (Telefono.value.length < 1 || Telefono.value.trim() == "") { // Si el campo está vacío o contiene solo espacios en blanco
    document.getElementById("T-error").innerHTML = "Numero de telefono no valido"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  }
  const correo = document.getElementById("correo"); // Se obtiene el valor del campo "correo"
  if (correo.value.length < 1 || correo.value.trim() == "") { // Si el campo está vacío o contiene solo espacios en blanco
    document.getElementById("c-error").innerHTML = "Correo electrónico no válido"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  }
  const genero = document.getElementById("genero"); // Se obtiene el valor del campo "genero"
  if (genero.value.length < 1 || genero.value.trim() == "") { // Si el campo está vacío o contiene solo espacios en blanco
    document.getElementById("g-error").innerHTML = "Genero no válida"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  }

  const direccion = document.getElementById("direccion"); // Se obtiene el valor del campo "direccion"
  if (direccion.value.length < 1 || direccion.value.trim() == "") { // Si el campo está vacío o contiene solo espacios en blanco
    document.getElementById("d-error").innerHTML = "Ingrese una dirección valida"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  }
  const terminos = document.getElementById("terminos"); // Se obtiene el valor del campo "terminos"
  if (!terminos.checked) { // Si el campo no está seleccionado
    document.getElementById("t-error").innerHTML = "Por favor, acepte los términos y condiciones"; // Se muestra un mensaje de error
    condicion = false; // Se cambia la variable condicion a falso
  } else {
    document.getElementById("t-error").innerHTML = ""; // Si el campo está seleccionado, se borra cualquier mensaje de error previo
  }
  return condicion; // Se retorna el valor de la variable condicion
}

